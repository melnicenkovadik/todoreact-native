import React, {useState} from 'react';
import {Text, TextInput, View, StyleSheet, SectionList} from 'react-native';
import Todo from './Todo';
import Filtered from './Filtered';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    color: 'black',
    backgroundColor: 'red',
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
const App = () => {
  const [inputValue, setInputValue] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [todos, setTodos] = useState([
    {
      text: 'Learn Reactjs today',
      isCompleted: true,
      date: new Date('Fri Apr 15 2021 03:00:00 GMT+0300'),
    },
    {
      text: 'Buy meat from the market',
      isCompleted: true,
      date: new Date('04-17-2021-10:00'),
    },
    {
      text: "Go to friend's house",
      isCompleted: true,
      date: new Date('04-27-2021-10:00'),
    },
  ]);
  const addTodo = () => {
    const newTodos = [...todos, {text: inputValue, date: new Date(Date.now())}];
    setTodos(newTodos);
    setSearchTodos(newTodos);
    setInputValue('');
  };
  const [searchTodos, setSearchTodos] = useState([...todos]);

  const searchHandler = () => {
    if (!searchValue.length || searchTodos === [] || todos) {
      setSearchTodos(todos);
    }
    const newTodos = [...todos];
    const filtered = newTodos.filter(
      item =>
        item.text
          .toLowerCase()
          .trim()
          .indexOf(searchValue.toLowerCase().trim()) !== -1,
    );
    setSearchTodos(filtered);
  };

  const removeTodo = index => {
    searchHandler();
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
    setSearchTodos(newTodos);
  };

  const todo = {
    searchValue,
    setSearchValue,
    searchHandler,
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      <Filtered {...todo} />
      <SectionList
        style={{
          flex: 8,
        }}
        sections={[
          {
            title: <Text style={{fontSize: 96}}>Todo list</Text>,
            data: [...searchTodos],
          },
        ]}
        renderItem={todo => (
          <Todo
            removeTodo={removeTodo}
            todo={todo.item}
            index={todo.item.index}
          />
        )}
        keyExtractor={(item, index) => index}
      />
      <TextInput
        onChangeText={e => setInputValue(e)}
        onSubmitEditing={addTodo}
        style={{
          borderWidth: 1,
        }}
        defaultValue={inputValue}
      />
    </View>
  );
};

export default App;
