import React from 'react';
import {Button, TextInput, View} from 'react-native';

const Filtered = ({searchValue, setSearchValue, searchHandler}) => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <TextInput
          style={{
            borderWidth: 1,
          }}
          type="text"
          placeholder="Search..."
          onChangeText={event => setSearchValue(event)}
          className="input-field"
          value={searchValue}
        />
        <Button onPress={searchHandler} className="btn" title={'Search'} />
      </View>
    </View>
  );
};

export default Filtered;
