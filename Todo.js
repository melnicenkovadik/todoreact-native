import React from 'react';
import {Button, View, StyleSheet, Text, ScrollView} from 'react-native';
import LogBoxButton from 'react-native/Libraries/LogBox/UI/LogBoxButton';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  containerItem: {
    flex: 1,
    paddingTop: 22,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  text: {
    flex: 1,
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  button: {
    flex: 1,
    width: '100%',
    backgroundColor: 'blue',
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
const Todo = ({todo, index, completeTodo, removeTodo}) => {
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerItem}>
        <Text style={styles.text}>{todo.text}</Text>
        <LogBoxButton>
          <Button
            style={styles.button}
            onPress={() => removeTodo(index)}
            title={'X'}
          />
        </LogBoxButton>
      </View>
    </ScrollView>
  );
};

export default Todo;
